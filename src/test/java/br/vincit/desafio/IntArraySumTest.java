package br.vincit.desafio;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("TestesDigitoUnico")
class IntArraySumTest {
	
	private IntArraySum app;
	
	@BeforeEach
	public void init() {
		app = new IntArraySum();
	}

	@Test
	@DisplayName("calc with length array eq 9")
	void testWithLengthArrayEq9() {
		
		Integer sum = app.sumIntArray(Arrays.asList(1,2,11,4,5,6,7,8,9));
		
		assertEquals(53, sum);

	}

}
