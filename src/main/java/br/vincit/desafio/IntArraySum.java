package br.vincit.desafio;

import java.util.List;
import java.util.logging.Logger;

public class IntArraySum {

	private static final Logger logger = Logger.getLogger(IntArraySum.class.getName());
	
	public Integer sumIntArray(List<Integer> numeros) {
		return numeros.stream().reduce(0, Integer::sum);
	}

}
